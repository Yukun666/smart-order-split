from flask import Flask, request, jsonify, make_response
from invest_v2 import *
from cerberus import Validator
import json
import copy

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

etf = stock()
pf = portfolio()


@app.route('/investment/', methods=['GET', 'POST'])
def investment_api():
    input = request.get_json()
    with open('./schema.json') as f:
        schema = json.load(f)
    v = Validator(schema, require_all=True)

    v.validate(input, schema)
    # try:
    #     # details, ptfls, orderAmt, amount_precision, unit_precision = read_json(input)
    #     details = input["goals"][0]['goalDetails']
    #     ptfls = input["goals"][0]['modelPortfolioDetails']
    #     orderAmt = float(input["goals"][0]['orderAmount'])
    #     amount_precision = int(input['amountDecimalPrecision'])
    #     unit_precision = int(input['unitDecimalPrecision'])
    if not bool(v.errors):
        print('Input type error')
        output_input_error = {}
        body = {}
        body['errorType'] = 'VALIDATION_ERROR'
        body['message'] = 'input typeError' + v.errors
        output_input_error['response body'] = body
        output_input_error['statusCode'] = 400
        return make_response(jsonify(output_input_error))

    else:
        details, ptfls, orderAmt, amount_precision, unit_precision = read_json(input)
        tcs = readDetail(details)
        pfs = readPtfls(ptfls)
        tcsComp = complete(tcs, pfs)
        try:
            result, status = investOptim(tcsComp, orderAmt)
        except:
            print('invest api error')
            output_api_error = {}
            body_api_error = {}
            body_api_error['errorType'] = 'API_ERROR'
            body_api_error['message'] = str(status.message)

            output_api_error['response body'] = body_api_error
            output_api_error['statusCode'] = 500

            return make_response(jsonify(output_api_error))
        # return tcsComp
        else:
            output = {}
            transactionDetail = []
            output['goalID'] = input['goals'][0]['goalId']
            output['transactionType'] = 'investment'

            if status.success == False:
                output['message'] = str(status.message)
            tickers = [i.ticker for i in tcsComp]
            for ind, val in enumerate(tickers):
                tmp = {}
                tmp['ticker'] = val
                tmp['direction'] = 'BUY'
                tmp['value'] = str(round(result[ind], amount_precision))
                tmp['units'] = str(round(result[ind], amount_precision) / tcsComp[ind].marketprice)
                transactionDetail.append(tmp)

            output['transactionDetail'] = transactionDetail
            output['statusCode'] = 200

            return make_response(jsonify([output]))


@app.route('/redemption/', methods=['GET', 'POST'])
def redemption_api():
    input = request.get_json()
    output = {}
    with open('./schema.json') as f:
        schema = json.load(f)
    v = Validator(schema, require_all=True)

    v.validate(input, schema)
    # details, ptfls, orderAmt, amount_precision, unit_precision = read_json(input)
    if not bool(v.errors):
        print('Input type error')

        output_input_error = {}
        body = {}
        body['errorType'] = 'VALIDATION_ERROR'
        body['message'] = 'input typeError' + v.errors
        output_input_error['response body'] = body
        output_input_error['statusCode'] = 400
        return make_response(jsonify(output_input_error))
    else:
        details, ptfls, orderAmt, amount_precision, unit_precision = read_json(input)
        tcs = readDetail(details)
        pfs = readPtfls(ptfls)
        tcsComp = complete(tcs, pfs)

        _, _, _, _, result, _, status = redempt(tcsComp, orderAmt)
        # return tcsComp

        transactionDetail = []
        output['goalID'] = input['goals'][0]['goalId']
        output['transactionType'] = 'redemption'

        if status.success == False:
            output['message'] = status.message

        tickers = [i.ticker for i in tcsComp]

        for ind, val in enumerate(tickers):
            tmp = {}
            tmp['ticker'] = val
            tmp['direction'] = 'DROP'
            tmp['value'] = str(round(result[ind], amount_precision))
            tmp['units'] = str(round(result[ind], amount_precision) / tcsComp[ind].marketprice)
            transactionDetail.append(tmp)

        output['transactionDetail'] = transactionDetail
        output['statusCode'] = 200

        return make_response(jsonify([output]))


@app.route('/switch/', methods=['GET', 'POST'])
def switch_api():
    input = request.get_json()
    pay_from = input['from']
    pay_to = input['to']
    details_1, ptfls_1, orderAmt_1, amount_precision_1, unit_precision_1 = read_json(pay_from)
    details_2, ptfls_2, _, amount_precision_2, unit_precision_2 = read_json(pay_to)

    tcsComp1 = transfer_to_stock(details_1, ptfls_1)
    tcsComp2 = transfer_to_stock(details_2, ptfls_2)

    _, result, _, _, _, _, e1, e2 = switch(tcsComp1, tcsComp2, orderAmt_1)

    output = {}
    switch_redemption = []
    switch_invest = []
    tickers = [i.ticker for i in tcsComp1]

    for ind, val in enumerate(tickers):
        print(type(ind), type(amount_precision_1))
        tmp_redemption = {}
        tmp_invest = {}
        tmp_redemption['ticker'] = val
        tmp_redemption['direction'] = 'DROP'
        tmp_redemption['value'] = str(round(result[ind], amount_precision_1))
        tmp_redemption['units'] = str(round(result[ind], amount_precision_1) / tcsComp1[ind].marketprice)
        switch_redemption.append(tmp_redemption)

        tmp_invest['ticker'] = val
        tmp_invest['direction'] = 'BUY'
        tmp_invest['value'] = str(round(result[ind], amount_precision_2))
        tmp_invest['units'] = str(round(result[ind], amount_precision_2) / tcsComp2[ind].marketprice)
        switch_invest.append(tmp_invest)

    output['switch_redemption'] = switch_redemption
    # output['switch_invest'] = switch_invest
    output['statusCode'] = 200

    return make_response(jsonify(output))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105, debug=True, use_reloader=False)
